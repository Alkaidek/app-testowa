module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    // ecmaVersion: 2016,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
  },
  extends: ['plugin:vue/strongly-recommended', 'plugin:prettier/recommended'],
  globals: {
    __static: true,
  },
  plugins: ['vue', 'prettier'],
  rules: {
    'prettier/prettier': 2,
    semi: 0,
    // end comma in array
    'comma-dangle': 0,
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-console': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // vue rules
    'vue/html-closing-bracket-newline': [
      'error',
      {
        singleline: 'never',
        multiline: 'always',
      },
    ],
    'vue/html-closing-bracket-spacing': [
      'error',
      {
        startTag: 'never',
        endTag: 'never',
        selfClosingTag: 'always',
      },
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'never',
          component: 'never',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/max-attributes-per-line': [
      'off',
      {
        singleline: 1,
        multiline: {
          max: 1,
          allowFirstLine: false,
        },
      },
    ],
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': [
      'off',
      {
        ignores: ['pre', 'textarea'],
      },
    ],
    'vue/name-property-casing': ['error', 'kebab-case'],
    'vue/component-name-in-template-casing': ['error', 'kebab-case'],
    'vue/html-indent': [
      'error',
      2,
      {
        attribute: 1,
        closeBracket: 0,
        alignAttributesVertically: true,
        ignores: [],
      },
    ],
  },

  //  overrides: [
  //    {
  //      files: [ '*.vue' ],
  //      excludedFiles: '*.spec.js',
  //      rules: {
  //        'unicode-bom': 0,
  //        'no-trailing-spaces': 0,
  //        'max-lines': 0,
  //        'max-len': 0,
  //        'linebreak-style': 0,
  //        'eol-last': 0,
  //      }
  //   }
  // ]
};
