/**
 * Temporary Fix prettier error for version>1.13.7
 * in package.json add this
  "scripts": {
  "test:unit": "vue-cli-service test:unit --require tests/unit/setup.js"
}
 */
require('jsdom-global')();

window.Date = Date;
