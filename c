[33mcommit f14b52e7faa3fcdc87e040603e449340b8b82969[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Alkaidek <35803025+Alkaidek@users.noreply.github.com>
Date:   Mon Nov 26 11:05:55 2018 +0100

    init

[1mdiff --git a/.gitignore b/.gitignore[m
[1mnew file mode 100644[m
[1mindex 0000000..185e663[m
[1m--- /dev/null[m
[1m+++ b/.gitignore[m
[36m@@ -0,0 +1,21 @@[m
[32m+[m[32m.DS_Store[m
[32m+[m[32mnode_modules[m
[32m+[m[32m/dist[m
[32m+[m
[32m+[m[32m# local env files[m
[32m+[m[32m.env.local[m
[32m+[m[32m.env.*.local[m
[32m+[m
[32m+[m[32m# Log files[m
[32m+[m[32mnpm-debug.log*[m
[32m+[m[32myarn-debug.log*[m
[32m+[m[32myarn-error.log*[m
[32m+[m
[32m+[m[32m# Editor directories and files[m
[32m+[m[32m.idea[m
[32m+[m[32m.vscode[m
[32m+[m[32m*.suo[m
[32m+[m[32m*.ntvs*[m
[32m+[m[32m*.njsproj[m
[32m+[m[32m*.sln[m
[32m+[m[32m*.sw*[m
[1mdiff --git a/README.md b/README.md[m
[1mnew file mode 100644[m
[1mindex 0000000..d962bfc[m
[1m--- /dev/null[m
[1m+++ b/README.md[m
[36m@@ -0,0 +1,26 @@[m
[32m+[m[32m# app-testowa[m
[32m+[m
[32m+[m[32m## Project setup[m
[32m+[m[32m```[m
[32m+[m[32myarn install[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Compiles and hot-reloads for development[m
[32m+[m[32m```[m
[32m+[m[32myarn run serve[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Compiles and minifies for production[m
[32m+[m[32m```[m
[32m+[m[32myarn run build[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Lints and fixes files[m
[32m+[m[32m```[m
[32m+[m[32myarn run lint[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Run your unit tests[m
[32m+[m[32m```[m
[32m+[m[32myarn run test:unit[m
[32m+[m[32m```[m
[1mdiff --git a/babel.config.js b/babel.config.js[m
[1mnew file mode 100644[m
[1mindex 0000000..b89d655[m
[1m--- /dev/null[m
[1m+++ b/babel.config.js[m
[36m@@ -0,0 +1,5 @@[m
[32m+[m[32mmodule.exports = {[m
[32m+[m[32m  presets: [[m
[32m+[m[32m    '@vue/app'[m
[32m+[m[32m  ][m
[32m+[m[32m}[m
\ No newline at end of file[m
[1mdiff --git a/package.json b/package.json[m
[1mnew file mode 100644[m
[1mindex 0000000..b8d8031[m
[1m--- /dev/null[m
[1m+++ b/package.json[m
[36m@@ -0,0 +1,52 @@[m
[32m+[m[32m{[m
[32m+[m[32m  "name": "app-testowa",[m
[32m+[m[32m  "version": "0.1.0",[m
[32m+[m[32m  "private": true,[m
[32m+[m[32m  "scripts": {[m
[32m+[m[32m    "serve": "vue-cli-service serve",[m
[32m+[m[32m    "build": "vue-cli-service build",[m
[32m+[m[32m    "lint": "vue-cli-service lint",[m
[32m+[m[32m    "test:unit": "vue-cli-service test:unit"[m
[32m+[m[32m  },[m
[32m+[m[32m  "dependencies": {[m
[32m+[m[32m    "vue": "^2.5.17"[m
[32m+[m[32m  },[m
[32m+[m[32m  "devDependencies": {[m
[32m+[m[32m    "@vue/cli-plugin-babel": "^3.1.2",[m
[32m+[m[32m    "@vue/cli-plugin-eslint": "^3.1.2",[m
[32m+[m[32m    "@vue/cli-plugin-unit-mocha": "^3.1.2",[m
[32m+[m[32m    "@vue/cli-service": "^3.1.2",[m
[32m+[m[32m    "@vue/test-utils": "^1.0.0-beta.20",[m
[32m+[m[32m    "babel-eslint": "^10.0.1",[m
[32m+[m[32m    "chai": "^4.1.2",[m
[32m+[m[32m    "eslint": "^5.8.0",[m
[32m+[m[32m    "eslint-plugin-vue": "^5.0.0-0",[m
[32m+[m[32m    "node-sass": "^4.9.0",[m
[32m+[m[32m    "sass-loader": "^7.0.1",[m
[32m+[m[32m    "vue-template-compiler": "^2.5.17"[m
[32m+[m[32m  },[m
[32m+[m[32m  "eslintConfig": {[m
[32m+[m[32m    "root": true,[m
[32m+[m[32m    "env": {[m
[32m+[m[32m      "node": true[m
[32m+[m[32m    },[m
[32m+[m[32m    "extends": [[m
[32m+[m[32m      "plugin:vue/essential",[m
[32m+[m[32m      "eslint:recommended"[m
[32m+[m[32m    ],[m
[32m+[m[32m    "rules": {},[m
[32m+[m[32m    "parserOptions": {[m
[32m+[m[32m      "parser": "babel-eslint"[m
[32m+[m[32m    }[m
[32m+[m[32m  },[m
[32m+[m[32m  "postcss": {[m
[32m+[m[32m    "plugins": {[m
[32m+[m[32m      "autoprefixer": {}[m
[32m+[m[32m    }[m
[32m+[m[32m  },[m
[32m+[m[32m  "browserslist": [[m
[32m+[m[32m    "> 1%",[m
[32m+[m[32m    "last 2 versions",[m
[32m+[m[32m    "not ie <= 8"[m
[32m+[m[32m  ][m
[32m+[m[32m}[m
\ No newline at end of file[m
[1mdiff --git a/public/favicon.ico b/public/favicon.ico[m
[1mnew file mode 100644[m
[1mindex 0000000..c7b9a43[m
Binary files /dev/null and b/public/favicon.ico differ
[1mdiff --git a/public/index.html b/public/index.html[m
[1mnew file mode 100644[m
[1mindex 0000000..a1f3559[m
[1m--- /dev/null[m
[1m+++ b/public/index.html[m
[36m@@ -0,0 +1,17 @@[m
[32m+[m[32m<!DOCTYPE html>[m
[32m+[m[32m<html lang="en">[m
[32m+[m[32m  <head>[m
[32m+[m[32m    <meta charset="utf-8">[m
[32m+[m[32m    <meta http-equiv="X-UA-Compatible" content="IE=edge">[m
[32m+[m[32m    <meta name="viewport" content="width=device-width,initial-scale=1.0">[m
[32m+[m[32m    <link rel="icon" href="<%= BASE_URL %>favicon.ico">[m
[32m+[m[32m    <title>app-testowa</title>[m
[32m+[m[32m  </head>[m
[32m+[m[32m  <body>[m
[32m+[m[32m    <noscript>[m
[32m+[m[32m      <strong>We're sorry but app-testowa doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>[m
[32m+[m[32m    </noscript>[m
[32m+[m[32m    <div id="app"></div>[m
[32m+[m[32m    <!-- built files will be auto injected -->[m
[32m+[m[32m  </body>[m
[32m+[m[32m</html>[m
[1mdiff --git a/src/App.vue b/src/App.vue[m
[1mnew file mode 100644[m
[1mindex 0000000..2913e30[m
[1m--- /dev/null[m
[1m+++ b/src/App.vue[m
[36m@@ -0,0 +1,28 @@[m
[32m+[m[32m<template>[m
[32m+[m[32m  <div id="app">[m
[32m+[m[32m    <img alt="Vue logo" src="./assets/logo.png">[m
[32m+[m[32m    <hello msg="Welcome to Your Vue.js App"/>[m
[32m+[m[32m  </div>[m
[32m+[m[32m</template>[m
[32m+[m
[32m+[m[32m<script>[m
[32m+[m[32mimport hello from './components/hello.vue'[m
[32m+[m
[32m+[m[32mexport default {[m
[32m+[m[32m  name: 'app',[m
[32m+[m[32m  components: {[m
[32m+[m[32m    HelloWorld[m
[32m+[m[32m  }[m
[32m+[m[32m}[m
[32m+[m[32m</script>[m
[32m+[m
[32m+[m[32m<style lang="scss">[m
[32m+[m[32m#app {[m
[32m+[m[32m  font-family: 'Avenir', Helvetica, Arial, sans-serif;[m
[32m+[m[32m  -webkit-font-smoothing: antialiased;[m
[32m+[m[32m  -moz-osx-font-smoothing: grayscale;[m
[32m+[m[32m  text-align: center;[m
[32m+[m[32m  color: #2c3e50;[m
[32m+[m[32m  margin-top: 60px;[m
[32m+[m[32m}[m
[32m+[m[32m</style>[m
[1mdiff --git a/src/assets/logo.png b/src/assets/logo.png[m
[1mnew file mode 100644[m
[1mindex 0000000..f3d2503[m
Binary files /dev/null and b/src/assets/logo.png differ
[1mdiff --git a/src/components/hello.vue b/src/components/hello.vue[m
[1mnew file mode 100644[m
[1mindex 0000000..b34d2a3[m
[1m--- /dev/null[m
[1m+++ b/src/components/hello.vue[m
[36m@@ -0,0 +1,59 @@[m
[32m+[m[32m<template>[m
[32m+[m[32m  <div class="hello">[m
[32m+[m[32m    <h1>{{ msg }}</h1>[m
[32m+[m[32m    <p>[m
[32m+[m[32m      For a guide and recipes on how to configure / customize this project,<br>[m
[32m+[m[32m      check out the[m
[32m+[m[32m      <a href="https://cli.vuejs.org" target="_blank" rel="noopener">vue-cli documentation</a>.[m
[32m+[m[32m    </p>[m
[32m+[m[32m    <h3>Installed CLI Plugins</h3>[m
[32m+[m[32m    <ul>[m
[32m+[m[32m      <li><a href="https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-babel" target="_blank" rel="noopener">babel</a></li>[m
[32m+[m[32m      <li><a href="https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-eslint" target="_blank" rel="noopener">eslint</a></li>[m
[32m+[m[32m      <li><a href="https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-unit-mocha" target="_blank" rel="noopener">unit-mocha</a></li>[m
[32m+[m[32m    </ul>[m
[32m+[m[32m    <h3>Essential Links</h3>[m
[32m+[m[32m    <ul>[m
[32m+[m[32m      <li><a href="https://vuejs.org" target="_blank" rel="noopener">Core Docs</a></li>[m
[32m+[m[32m      <li><a href="https://forum.vuejs.org" target="_blank" rel="noopener">Forum</a></li>[m
[32m+[m[32m      <li><a href="https://chat.vuejs.org" target="_blank" rel="noopener">Community Chat</a></li>[m
[32m+[m[32m      <li><a href="https://twitter.com/vuejs" target="_blank" rel="noopener">Twitter</a></li>[m
[32m+[m[32m      <li><a href="https://news.vuejs.org" target="_blank" rel="noopener">News</a></li>[m
[32m+[m[32m    </ul>[m
[32m+[m[32m    <h3>Ecosystem</h3>[m
[32m+[m[32m    <ul>[m
[32m+[m[32m      <li><a href="https://router.vuejs.org" target="_blank" rel="noopener">vue-router</a></li>[m
[32m+[m[32m      <li><a href="https://vuex.vuejs.org" target="_blank" rel="noopener">vuex</a></li>[m
[32m+[m[32m      <li><a href="https://github.com/vuejs/vue-devtools#vue-devtools" target="_blank" rel="noopener">vue-devtools</a></li>[m
[32m+[m[32m      <li><a href="https://vue-loader.vuejs.org" target="_blank" rel="noopener">vue-loader</a></li>[m
[32m+[m[32m      <li><a href="https://github.com/vuejs/awesome-vue" target="_blank" rel="noopener">awesome-vue</a></li>[m
[32m+[m[32m    </ul>[m
[32m+[m[32m  </div>[m
[32m+[m[32m</template>[m
[32m+[m
[32m+[m[32m<script>[m
[32m+[m[32mexport default {[m
[32m+[m[32m  name: 'hello',[m
[32m+[m[32m  props: {[m
[32m+[m[32m    msg: String[m
[32m+[m[32m  }[m
[32m+[m[32m}[m
[32m+[m[32m</script>[m
[32m+[m
[32m+[m[32m<!-- Add "scoped" attribute to limit CSS to this component only -->[m
[32m+[m[32m<style scoped lang="scss">[m
[32m+[m[32mh3 {[m
[32m+[m[32m  margin: 40px 0 0;[m
[32m+[m[32m}[m
[32m+[m[32mul {[m
[32m+[m[32m  list-style-type: none;[m
[32m+[m[32m  padding: 0;[m
[32m+[m[32m}[m
[32m+[m[32mli {[m
[32m+[m[32m  display: inline-block;[m
[32m+[m[32m  margin: 0 10px;[m
[32m+[m[32m}[m
[32m+[m[32ma {[m
[32m+[m[32m  color: #42b983;[m
[32m+[m[32m}[m
[32m+[m[32m</style>[m
[1mdiff --git a/src/main.js b/src/main.js[m
[1mnew file mode 100644[m
[1mindex 0000000..63eb05f[m
[1m--- /dev/null[m
[1m+++ b/src/main.js[m
[36m@@ -0,0 +1,8 @@[m
[32m+[m[32mimport Vue from 'vue'[m
[32m+[m[32mimport App from './App.vue'[m
[32m+[m
[32m+[m[32mVue.config.productionTip = false[m
[32m+[m
[32m+[m[32mnew Vue({[m
[32m+[m[32m  render: h => h(App),[m
[32m+[m[32m}).$mount('#app')[m
[1mdiff --git a/tests/unit/.eslintrc.js b/tests/unit/.eslintrc.js[m
[1mnew file mode 100644[m
[1mindex 0000000..c00d243[m
[1m--- /dev/null[m
[1m+++ b/tests/unit/.eslintrc.js[m
[36m@@ -0,0 +1,5 @@[m
[32m+[m[32mmodule.exports = {[m
[32m+[m[32m  env: {[m
[32m+[m[32m    mocha: true[m
[32m+[m[32m  }[m
[32m+[m[32m}[m
\ No newline at end of file[m
[1mdiff --git a/tests/unit/example.spec.js b/tests/unit/example.spec.js[m
[1mnew file mode 100644[m
[1mindex 0000000..c585ea3[m
[1m--- /dev/null[m
[1m+++ b/tests/unit/example.spec.js[m
[36m@@ -0,0 +1,13 @@[m
[32m+[m[32mimport { expect } from 'chai'[m
[32m+[m[32mimport { shallowMount } from '@vue/test-utils'[m
[32m+[m[32mimport hello from '@/components/hello.vue'[m
[32m+[m
[32m+[m[32mdescribe('hello.vue', () => {[m
[32m+[m[32m  it('renders props.msg when passed', () => {[m
[32m+[m[32m    const msg = 'new message'[m
[32m+[m[32m    const wrapper = shallowMount(hello, {[m
[32m+[m[32m      propsData: { msg }[m
[32m+[m[32m    })[m
[32m+[m[32m    expect(wrapper.text()).to.include(msg)[m
[32m+[m[32m  })[m
[32m+[m[32m})[m
[1mdiff --git a/yarn.lock b/yarn.lock[m
[1mnew file mode 100644[m
[1mindex 0000000..0464fde[m
[1m--- /dev/null[m
[1m+++ b/yarn.lock[m
[36m@@ -0,0 +1,7452 @@[m
[32m+[m[32m# THIS IS AN AUTOGENERATED FILE. DO NOT EDIT THIS FILE DIRECTLY.[m
[32m+[m[32m# yarn lockfile v1[m
[32m+[m
[32m+[m
[32m+[m[32m"@babel/code-frame@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/code-frame/-/code-frame-7.0.0.tgz#06e2ab19bdb535385559aabb5ba59729482800f8"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/highlight" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/core@^7.0.0":[m
[32m+[m[32m  version "7.1.6"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/core/-/core-7.1.6.tgz#3733cbee4317429bc87c62b29cf8587dba7baeb3"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/code-frame" "^7.0.0"[m
[32m+[m[32m    "@babel/generator" "^7.1.6"[m
[32m+[m[32m    "@babel/helpers" "^7.1.5"[m
[32m+[m[32m    "@babel/parser" "^7.1.6"[m
[32m+[m[32m    "@babel/template" "^7.1.2"[m
[32m+[m[32m    "@babel/traverse" "^7.1.6"[m
[32m+[m[32m    "@babel/types" "^7.1.6"[m
[32m+[m[32m    convert-source-map "^1.1.0"[m
[32m+[m[32m    debug "^4.1.0"[m
[32m+[m[32m    json5 "^2.1.0"[m
[32m+[m[32m    lodash "^4.17.10"[m
[32m+[m[32m    resolve "^1.3.2"[m
[32m+[m[32m    semver "^5.4.1"[m
[32m+[m[32m    source-map "^0.5.0"[m
[32m+[m
[32m+[m[32m"@babel/generator@^7.1.6":[m
[32m+[m[32m  version "7.1.6"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/generator/-/generator-7.1.6.tgz#001303cf87a5b9d093494a4bf251d7b5d03d3999"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.1.6"[m
[32m+[m[32m    jsesc "^2.5.1"[m
[32m+[m[32m    lodash "^4.17.10"[m
[32m+[m[32m    source-map "^0.5.0"[m
[32m+[m[32m    trim-right "^1.0.1"[m
[32m+[m
[32m+[m[32m"@babel/helper-annotate-as-pure@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-annotate-as-pure/-/helper-annotate-as-pure-7.0.0.tgz#323d39dd0b50e10c7c06ca7d7638e6864d8c5c32"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-builder-binary-assignment-operator-visitor@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-builder-binary-assignment-operator-visitor/-/helper-builder-binary-assignment-operator-visitor-7.1.0.tgz#6b69628dfe4087798e0c4ed98e3d4a6b2fbd2f5f"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-explode-assignable-expression" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-call-delegate@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-call-delegate/-/helper-call-delegate-7.1.0.tgz#6a957f105f37755e8645343d3038a22e1449cc4a"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-hoist-variables" "^7.0.0"[m
[32m+[m[32m    "@babel/traverse" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-define-map@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-define-map/-/helper-define-map-7.1.0.tgz#3b74caec329b3c80c116290887c0dd9ae468c20c"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-function-name" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m[32m    lodash "^4.17.10"[m
[32m+[m
[32m+[m[32m"@babel/helper-explode-assignable-expression@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-explode-assignable-expression/-/helper-explode-assignable-expression-7.1.0.tgz#537fa13f6f1674df745b0c00ec8fe4e99681c8f6"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/traverse" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-function-name@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-function-name/-/helper-function-name-7.1.0.tgz#a0ceb01685f73355d4360c1247f582bfafc8ff53"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-get-function-arity" "^7.0.0"[m
[32m+[m[32m    "@babel/template" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-get-function-arity@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-get-function-arity/-/helper-get-function-arity-7.0.0.tgz#83572d4320e2a4657263734113c42868b64e49c3"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-hoist-variables@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-hoist-variables/-/helper-hoist-variables-7.0.0.tgz#46adc4c5e758645ae7a45deb92bab0918c23bb88"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-member-expression-to-functions@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-member-expression-to-functions/-/helper-member-expression-to-functions-7.0.0.tgz#8cd14b0a0df7ff00f009e7d7a436945f47c7a16f"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-module-imports@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-module-imports/-/helper-module-imports-7.0.0.tgz#96081b7111e486da4d2cd971ad1a4fe216cc2e3d"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-module-transforms@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-module-transforms/-/helper-module-transforms-7.1.0.tgz#470d4f9676d9fad50b324cdcce5fbabbc3da5787"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-module-imports" "^7.0.0"[m
[32m+[m[32m    "@babel/helper-simple-access" "^7.1.0"[m
[32m+[m[32m    "@babel/helper-split-export-declaration" "^7.0.0"[m
[32m+[m[32m    "@babel/template" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m[32m    lodash "^4.17.10"[m
[32m+[m
[32m+[m[32m"@babel/helper-optimise-call-expression@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-optimise-call-expression/-/helper-optimise-call-expression-7.0.0.tgz#a2920c5702b073c15de51106200aa8cad20497d5"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-plugin-utils@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-plugin-utils/-/helper-plugin-utils-7.0.0.tgz#bbb3fbee98661c569034237cc03967ba99b4f250"[m
[32m+[m
[32m+[m[32m"@babel/helper-regex@^7.0.0":[m
[32m+[m[32m  version "7.0.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-regex/-/helper-regex-7.0.0.tgz#2c1718923b57f9bbe64705ffe5640ac64d9bdb27"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    lodash "^4.17.10"[m
[32m+[m
[32m+[m[32m"@babel/helper-remap-async-to-generator@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-remap-async-to-generator/-/helper-remap-async-to-generator-7.1.0.tgz#361d80821b6f38da75bd3f0785ece20a88c5fe7f"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-annotate-as-pure" "^7.0.0"[m
[32m+[m[32m    "@babel/helper-wrap-function" "^7.1.0"[m
[32m+[m[32m    "@babel/template" "^7.1.0"[m
[32m+[m[32m    "@babel/traverse" "^7.1.0"[m
[32m+[m[32m    "@babel/types" "^7.0.0"[m
[32m+[m
[32m+[m[32m"@babel/helper-replace-supers@^7.1.0":[m
[32m+[m[32m  version "7.1.0"[m
[32m+[m[32m  resolved "https://registry.yarnpkg.com/@babel/helper-replace-supers/-/helper-replace-supers-7.1.0.tgz#5fc31de522ec0ef0899dc9b3e7cf6a5dd655f362"[m
[32m+[m[32m  dependencies:[m
[32m+[m[32m    "@babel/helper-member-expression-to-functions" "^7.0.0"[m
[32m+[m[32m    "@babel/helper-optimise-call-expressi